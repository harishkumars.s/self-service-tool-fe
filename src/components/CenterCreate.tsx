import { useForm, SubmitHandler } from "react-hook-form";
import { useState } from 'react';

type Inputs = {
  apiKey: string;
  baseConfigId: string;
  name: string;
  state: string;
  TcId: string;
  TpId: string;
  district: string;
  centerHead: string;
  centerMobile: string;
  centerEmail: string;
  centerLocation: string;
  centerLocationURL: string;
  courses: string;
};

export default function CenterCreate() {

  const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    console.log(data);

    const body = JSON.stringify(data)
    console.log('body', body)
    const res = await fetch('/tenantCreate', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: body
    })

    if (res.ok) {
      const res2 = await res.json()
      res2.url = 'https://uaf-web-banner.in.affinidi.io/?apiKey=' + res2['sourcePlatformId']
      const output = JSON.stringify(res2, null, 4)
      console.log('output', output)
      setValue(output)
    } else {
      setValue('Center creation failed')
    }
  }

  const [newCenterInfo, setValue] = useState<string>('');

  return (
    <div>
      <h5 className='h5Title'>Create center</h5>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input className='input' placeholder='Admin API key' {...register('apiKey', { required: true })} />
        {errors.apiKey && <span>Admin API key is required</span>}
        <input className='input' placeholder='Base config ID' {...register('baseConfigId', { required: true })} />
        {errors.apiKey && <span>Base config ID is required</span>}
        <input className='input' placeholder='Name' {...register('name', { required: true })} />
        {errors.name && <span>Name is required</span>}
        <input className='input' placeholder='State' {...register('state', { required: true })} />
        {errors.state && <span>State is required</span>}
        <input className='input' placeholder='TCID' {...register('TcId', { required: true })} />
        {errors.TcId && <span>TCID is required</span>}
        <input className='input' placeholder='TPID' {...register('TpId', { required: true })} />
        {errors.TpId && <span>TPID is required</span>}
        <input className='input' placeholder='District' {...register('district', { required: true })} />
        {errors.district && <span>District is required</span>}
        <input className='input' placeholder='Center head' {...register('centerHead', { required: true })} />
        {errors.centerHead && <span>Center head is required</span>}
        <input className='input' placeholder='Center Mobile' {...register('centerMobile', { required: true })} />
        {errors.centerMobile && <span>Center Mobile is required</span>}
        <input className='input' placeholder='Center Email' {...register('centerEmail', { required: true })} />
        {errors.centerEmail && <span>Center Email is required</span>}
        <input className='input' placeholder='Center Address' {...register('centerLocation', { required: true })} />
        {errors.centerLocation && <span>Center Address is required</span>}
        <input className='input' placeholder='Center Location URL' {...register('centerLocationURL', { required: true })} />
        <textarea className='textarea2' placeholder='Courses (one per line)' {...register('courses', { required: true })} />
        {errors.courses && <span>At least one course is required</span>}

        <input value='Create' className='actionBtn' type="submit" />
      </form>
      <pre>{newCenterInfo}</pre>
    </div>
  );
}