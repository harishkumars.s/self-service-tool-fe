import { useForm, SubmitHandler } from "react-hook-form";
import { useState } from 'react';

type Inputs = {
  input: string,
};

export default function JsonFormatter() {

  const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      const jsonObj = JSON.parse(data.input);
      setValue(JSON.stringify(jsonObj, null, 4))
    } catch (e) {
      setValue('Invalid JSON')
    }
  }

  const [jsonFormatted, setValue] = useState<string>('');

  return (
    <div>
      <h5 className='h5Title'>JSON Formatter</h5>
      <form onSubmit={handleSubmit(onSubmit)}>
        <textarea className='textarea2' placeholder='JSON' {...register("input", { required: true })} />
        {errors.input && <span>JSON is required</span>}
        <input value='Format' className='actionBtn' type="submit" />
      </form>
      <pre>{jsonFormatted}</pre>
    </div>
  );
}