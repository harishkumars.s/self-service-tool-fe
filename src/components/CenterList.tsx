import { useForm, SubmitHandler } from "react-hook-form";
import React, {useState} from 'react';
import CustomTable from './CustomTable';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { userInfo } from "os";

type Inputs = {
    adminAPIKey: string,
    tenantID: string,
};

interface ICenter {
  id: string;
  name: string;
}

type Center = {
    id: string;
    name: string;
  }

export default function CenterList() {

    const { register, handleSubmit, watch, formState: { errors } } = useForm<Inputs>();

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        console.log(data);
        const body = JSON.stringify({ 'apiKey': data.adminAPIKey, 'tenantId': data.tenantID })
        console.log('body', body)
        const res = await fetch('/centerList', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: body
        })

        if (res.ok) {
            const res2 = await res.json()
            const output = JSON.stringify(res2, null, 4)
            console.log('output', output)
            setValue(res2)
        }
    }

    const [centerInfo, setValue] = useState<Center[]>([]);

    const columns: ColumnsType<ICenter> = [
        {
          key: 'id',
          title: 'ID',
          dataIndex: 'id',
        },
        {
          key: 'name',
          title: 'Name',
          dataIndex: 'name',
        },
      ];

    return (
        <div>
            <h5 className='h5Title'>List centers</h5>
            <form onSubmit={handleSubmit(onSubmit)}>
                <input className='input' placeholder='Admin API key' {...register("adminAPIKey", { required: true })} />
                {errors.adminAPIKey && <span>Admin API key is required</span>}

                <input placeholder='Tenant ID' className='input' {...register("tenantID", { required: true })} />
                {errors.tenantID && <span>Tenant ID is required</span>}

                <input value='List Centers' className='actionBtn' type="submit" />
            </form>
            <br></br>
            <Table<ICenter> columns={columns} dataSource={centerInfo} />
        </div>
    );
}