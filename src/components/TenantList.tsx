import { useForm, SubmitHandler } from "react-hook-form";
import {useState} from 'react';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

type Inputs = {
    adminAPIKey: string,
};

interface ITenant {
  id: string;
  name: string;
}


type Tenant = {
    id: string;
    name: string;
  }

export default function TenantList() {

    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        console.log(data);
        const body = JSON.stringify({ 'apiKey': data.adminAPIKey})
        console.log('body', body)
        const res = await fetch('/tenantList', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: body
        })

        if (res.ok) {
            const res2 = await res.json()
            const output = JSON.stringify(res2, null, 4)
            console.log('output', output)
            setValue(res2)
        }
    }

    const [tenantInfo, setValue] = useState<Tenant[]>([]);

    const columns: ColumnsType<ITenant> = [
        {
          key: 'id',
          title: 'ID',
          dataIndex: 'id',
        },
        {
          key: 'name',
          title: 'Name',
          dataIndex: 'name',
        },
      ];

    return (
        <div>
            <h5 className='h5Title'>List centers</h5>
            <form onSubmit={handleSubmit(onSubmit)}>
                <input className='input' placeholder='Admin API key' {...register("adminAPIKey", { required: true })} />
                {errors.adminAPIKey && <span>Admin API key is required</span>}
                <input value='List Tenants' className='actionBtn' type="submit" />
            </form>
            <br></br>
            <Table<ITenant> columns={columns} dataSource={tenantInfo} />
        </div>
    );
}