import * as React from 'react';
import Table from "rc-table";

type MyProps = {
    columns: any;
    data: any;
}

export default class CustomTable extends React.Component<MyProps> {
    render() {
        return (
            <Table 
                columns={this.props.columns}
                data={this.props.data}
                tableLayout='auto'
            />
        );
    }
}
