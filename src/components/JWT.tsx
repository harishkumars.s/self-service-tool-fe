import { useForm, SubmitHandler } from "react-hook-form";
import { useState } from 'react';

type Inputs = {
  input: string,
};

export default function JWT() {

  const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    console.log(data);
    const body = JSON.stringify({ 'jwt': data.input })
    console.log('body', body)
    const res = await fetch('/jwtDecode', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: body
    })

    if (res.ok) {
      const res2 = await res.json()
      const output = JSON.stringify(res2, null, 4)
      console.log('output', output)
      setValue(output)
    } else {
      setValue('invalid token')
    }
  }

  const [jwtDecoded, setValue] = useState<string>('');

  return (
    <div>
      <h5 className='h5Title'>JWT Decoder</h5>
      <form onSubmit={handleSubmit(onSubmit)}>
        <textarea className='textarea2' placeholder='JWT' {...register("input", { required: true })} />
        {errors.input && <span>JWT token is required</span>}
        <input value='Decode' className='actionBtn' type="submit" />
      </form>
      <pre>{jwtDecoded}</pre>
    </div>
  );
}