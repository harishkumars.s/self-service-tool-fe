import * as React from 'react';

type MyProps = {
  val: number;
}

export default class Counter extends React.Component<MyProps, {}> {

  state = {
    count: 0
  };

  constructor (props: MyProps) {
    super(props)
    console.log('props', props)
  }

  componentDidMount() {
    console.log('in component did mount')
    this.setState({
      count: (this.props.val + 1)
    });
  }


  increment = () => {
    this.setState({
      count: (this.state.count + 1)
    });
  };

  decrement = () => {
    this.setState({
      count: (this.state.count - 1)
    });
  };

  render () {
    return (
      <div>
        <h1>{this.state.count}</h1>
        <button onClick={this.increment}>Increment</button>
        <button onClick={this.decrement}>Decrement</button>
      </div>
    );
  }
}