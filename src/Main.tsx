import { Routes, Route } from 'react-router-dom';
import JWT from './components/JWT'
import JSONFormatter from './components/JsonFormatter'
import TenantList from './components/TenantList';
import CenterCreate from './components/CenterCreate';
import CenterList from './components/CenterList';
import ComingSoon from './components/ComingSoon';
import './css/SimpleForm.css'

const Main = () => {
  return (
    <div className='container'>
      <Routes>
        <Route path='/jwtDecoder' element={<JWT />} />
        <Route path='/tenantList' element={<TenantList />} />
        <Route path='/centerCreate' element={<CenterCreate />} />
        <Route path='/centerList' element={<CenterList />} />
        <Route path='/jsonFormatter' element={<JSONFormatter />} />
        <Route path='/fixCenter' element={<ComingSoon />} />
        <Route path='/tenantCreate' element={<ComingSoon />} />
      </Routes>
    </div>
  );
}
export default Main;
