import { Link } from 'react-router-dom';
import Main from './Main';

function App() {
  return (
      <div>
        <ul>
          <li><Link to='/jwtDecoder'>JWT decoder</Link></li>
          <li><Link to='/jsonFormatter'>JSON Formatter</Link></li>
          <li><Link to='/tenantList'>Tenant list</Link></li>
          <li><Link to='/centerCreate'>Create center</Link></li>
          <li><Link to='/centerList'>List centers</Link></li>
          <li><Link to='/tenantCreate'>Create tenant</Link></li>
          <li><Link to='/fixCenter'>Fix Center</Link></li>
        </ul>
        <hr />
        <Main />       
      </div>   
  )
}

export default App;
